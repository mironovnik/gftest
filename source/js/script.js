"use strict";
(function () {

    const calculateIncome = (investmentValue) => {
        const income = parseFloat(investmentValue * 0.085).toFixed(1);
        document.querySelector('.calculation__form-output-value').textContent = '≈ ' + income.toString() + ' $';
    };
    const calculateInvestment = () => {
        let investmentSum = rangeThumb.value;
        sumField.value = investmentSum.toString() + ' ' + '$';
        calculateIncome(investmentSum);
    };
    const rangeThumb = document.querySelector('.calculation__input-range');
    rangeThumb.addEventListener('mousedown', () => {
        rangeThumb.addEventListener('mousemove', calculateInvestment);
        rangeThumb.addEventListener('mouseup', () => {
            rangeThumb.removeEventListener('mousemove', calculateInvestment);
        });
    });
    const sumField = document.getElementById('investition_summ');
    sumField.addEventListener('change', () => {
        calculateIncome(parseInt(sumField.value));
    });


    const btnTable = document.querySelector('.tariff-plans__hide-button');
    const trList = document.querySelectorAll('tr:not([data-display])');
    const tariffWrappers = document.querySelectorAll('.tariff-plans__item-wrapper');
    btnTable.addEventListener('click', () => {
        [].forEach.call(tariffWrappers, (element) => {
            element.classList.toggle('special-margin');
        });
        [].forEach.call(trList, (element) => {
            const tdBorders = document.querySelectorAll('.tariff-plans__table td');
            [].forEach.call(tdBorders, (element) => {
                element.classList.toggle('borderBottom');
            });
            element.classList.toggle('active-row');
        })
    });

    const slider = document.querySelector('.slider__wrapper');
    let slideWidth = document.querySelector('.slider__item').offsetWidth;
    const showSlide = (offset, direction) => {
        slider.style.transition = 'transform 500ms ease-in-out';
        const currentTranslate = slider.style.transform.match(/(-[0-9]|[0-9])+/ig);
        let currentTranslateValue;
        if (currentTranslate) {
            currentTranslateValue = currentTranslate[0];
        } else {
            currentTranslateValue = 0;
        }
        let offsetTotal;
        if (direction === 'left') {
            offsetTotal = parseInt(currentTranslateValue) - parseInt(offset);
        } else {
            offsetTotal = parseInt(currentTranslateValue) + parseInt(offset);
        }
        console.log(slideWidth);
        slider.style.transform = 'translateX(' + parseInt(offsetTotal) + 'px)';
    };
    const switchSlide = (e) => {
        clearTimeout(intervalId);
        const target = e.target;
        const slideNumberPrev = document.querySelector('.current').getAttribute('data-slide');
        const slideNumberActive = target.getAttribute('data-slide');
        if (target.classList.contains('slider__control-dot')) {
            if (!target.classList.contains('current')) {
                const dots = document.querySelectorAll('.slider__control-dot');
                [].forEach.call(dots, (element) => {
                    element.classList.remove('current');
                });
                target.classList.add('current');
                let direction = 'right';
                if (slideNumberPrev - slideNumberActive < 0) {
                    direction = 'left';
                }
                showSlide(slideWidth * Math.abs(slideNumberPrev - slideNumberActive), direction);
            }
        }
        intervalId = setInterval(autoSwitchSlide, 5000)
    };
    const autoSwitchSlide = () => {

        let currentDot = parseInt(document.querySelector('.slider__control-dot.current').getAttribute('data-slide'));
        const dots = document.querySelectorAll('.slider__control-dot');
        const lastDot = parseInt(dots[dots.length - 1].getAttribute('data-slide'));
        let previousDot = currentDot;
        let direction = 'left';
        if (currentDot === lastDot) {
            previousDot = lastDot;
            currentDot = 1;
            direction = 'right';
        } else {
            currentDot++;
        }
        [].forEach.call(dots, (element) => {
            element.classList.remove('current');
        });
        document.querySelector('[data-slide="' + currentDot + '"]').classList.add('current');
        showSlide(slideWidth * Math.abs(previousDot - currentDot), direction);
    };
    let intervalId = setInterval(autoSwitchSlide, 5000);
    const dotsList = document.querySelector('.slider__dots');
    dotsList.addEventListener('click', switchSlide);
    window.addEventListener('resize', () => {
        slideWidth = document.querySelector('.slider__item').offsetWidth;
        let currentSlideNum = document.querySelector('.current').getAttribute('data-slide');
        slider.style.transition = 'none';
        slider.style.transform = 'translateX(0px)';
        if (parseInt(currentSlideNum) !== 1) {
            slider.style.transform = 'translateX(' + (-1 * slideWidth * (currentSlideNum - 1)) + 'px)';
        }
    });

})();


